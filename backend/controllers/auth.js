const { validationResult } = require('express-validator');

exports.login = async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(422).json({
            success: false,
            errors: errors.array()
        });
    }

    res.status(200).json({ message: 'Login controller' });
};

exports.signup = async (req, res) => {
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        return res.status(401).json({
            success: false,
            errors: errors.array()
        });
    }

    res.status(200).json({ message: 'Signup controller' });
};