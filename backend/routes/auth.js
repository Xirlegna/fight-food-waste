const express = require('express');
const { check } = require('express-validator');

const controller = require('../controllers/auth');
const User = require('../models/user');

const router = express.Router();

router.post(
    '/login',
    [
        check('email')
            .notEmpty().trim().escape().withMessage('The e-mail field is empty').bail()
            .isEmail().withMessage('The e-mail is invalid'),
        check('password')
            .notEmpty().trim().escape().withMessage('The password field is empty')
    ],
    controller.login
);

router.post(
    '/signup',
    check('username')
        .notEmpty().trim().escape().withMessage('The username field is empty'),
    check('email')
        .notEmpty().trim().escape().withMessage('The e-mail field is empty').bail()
        .isEmail().withMessage('The e-mail is invalid').bail()
        .custom((value, { req }) => {
            return User.findOne({ email: value }).then((userDoc) => {
                if (userDoc) {
                    return Promise.reject('E-mail address already exists');
                }
            })
        })
        .normalizeEmail(),
    check('password')
        .notEmpty().trim().escape().withMessage('The password field is empty'),
    check('passwordRepeat')
        .notEmpty().trim().escape().withMessage('The password repeat field is empty'),
    controller.signup
);

module.exports = router;